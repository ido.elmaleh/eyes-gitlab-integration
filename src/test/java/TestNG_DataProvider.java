import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.RectangleSize;
import com.applitools.eyes.StdoutLogHandler;
import com.applitools.eyes.TestResultsSummary;
import com.applitools.eyes.selenium.ClassicRunner;
import com.applitools.eyes.selenium.Configuration;
import com.applitools.eyes.selenium.Eyes;
import com.applitools.eyes.selenium.StitchMode;
import com.applitools.eyes.selenium.fluent.Target;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;


import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * Runs Applitools tests with TestNG DataProvider
 */
public class TestNG_DataProvider {

    private static BatchInfo batch;
    private static ClassicRunner runner;
    private Eyes eyes;
    private WebDriver driver;


    @DataProvider(name="websites")
    public Object[][] websites()
    {
        return new Object[][] {
                { "https://www.applitools.com", "Applitools" },
                { "https://www.google.com", "Google" },
        };
    }

    @BeforeTest
    public static void setBatch() {
        // Must be before ALL tests (at Class-level)
        batch = new BatchInfo();

        // Initialize the Runner for your test.
        runner = new ClassicRunner();

    }

    @BeforeMethod
    public void beforeTest() {

        // Initialize the eyes SDK
        eyes = new Eyes(runner);

        // Raise an error if no API Key has been found.
        if(isNullOrEmpty(System.getenv("APPLITOOLS_API_KEY"))) {
            throw new RuntimeException("No API Key found; Please set environment variable 'APPLITOOLS_API_KEY'.");
        }

        Configuration config = eyes.getConfiguration();

        // Set your personal Applitols API Key from your environment variables.
        config.setApiKey(System.getenv("APPLITOOLS_API_KEY"));
        config.setBatch(batch);
        config.setStitchMode(StitchMode.CSS);
        eyes.setConfiguration(config);

        // enable verbose logs, must be used after setting the config
        eyes.setLogHandler(new StdoutLogHandler(true));

        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
        options.addArguments("--no-sandbox"); // Bypass OS security model

        // Use Chrome browser
        driver = new ChromeDriver(options);

    }

    @Test(dataProvider = "websites")
    public void providerTest(String webpage, String testname) {

        driver.get(webpage);

        // Set AUT's name, test name and viewport size (width X height)
        // We have set it to 800 x 600 to accommodate various screens. Feel free to change it.
        eyes.open(driver, "TestNG", testname,
                new RectangleSize(800, 600));

        // Visual checkpoint #1 - Check the login page.
        eyes.check("check window fully", Target.window().fully());

        // End the test.
        eyes.closeAsync();
    }



    @AfterTest
    public void afterTest() {
        // Close the browser.
        driver.quit();

        // If the test was aborted before eyes.close was called, ends the test as aborted.
        eyes.abortAsync();

        // Wait and collect all test results
        TestResultsSummary allTestResults = runner.getAllTestResults();

        // Print results
        System.out.println(allTestResults);
    }
}

